<?php

spl_autoload_register(function ($className) {
	$filePath = str_replace("\\", DIRECTORY_SEPARATOR, __DIR__ . "\\" . 	$className . ".php");
	if (file_exists($filePath)) {
		include $filePath;
	}
});
$describ = array();
$db = new Dbinfo("tranina","tranina","neto0391");

if (isset($_GET["tableName"])){
	$describ = $db->getTableDescr($_GET["tableName"]);
	} 
$isHeaderOutput = 0;

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ДЗ к 4.4</title>
	<style type="text/css">
		table { 
			border-spacing: 0;
			border-collapse: collapse;
		}

		table td, table th {
			border: 1px solid black;
			padding: 5px; 
		}

		table th {
			background: lightgray;
		}

		
	</style>
</head>
<body>
	<h1>Анализ базы данных</h1>
	<form metod = "GET">
		<label for="tableName">Выберите таблицу:</label>
		<?php echo $db->getTablList() ?>  
		<input type="submit" value="Показать таблицу" />
	</form>
	<h2>Описание таблицы</h2>
	<table>
		<?php 
		foreach ($describ as $key => $value) {

			if ($isHeaderOutput == 0)  {
				$isHeaderOutput = 1;
				?>
				<tr> <?php
					foreach ($value as $key => $item) {
						?>	<th class="header"><?php echo $key; ?></th>
						<?php
					}
			?> </tr> 
			<?php } ?>
					<tr>
						<?php	
						foreach ($value as $key => $item) {
							?>	<td><?php echo $item; ?></td>
							<?php
						}
			?> </tr> <?php	  
		}
	?>			
	</table>	    
</body>
</html>