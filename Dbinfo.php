<?php

class Dbinfo{
	private $pdo = null;
	private $dbName = "";
	function __construct($dbName,$login,$password){
		$this->dbName = $dbName;
		$this->pdo = new PDO("mysql:host=localhost;dbname=" . $dbName, $login, $password);
		$this->pdo->query("SET NAMES UTF8");
	}
	public function getPdo(){
		return $this->pdo;
	}
	public function getTablList(){
		$res = $this->pdo->prepare("SHOW TABLES") ;
		$res->execute();	
		$arr = $res->fetchAll();
		$strSelect = "";
		foreach (	$arr  as &$value) {	
			
			$strSelect =$strSelect."<option value=".$value[0].">".$value[0]."</option>";
		}  
		return "<select name=\"tableName\">". $strSelect. "</select>";
	}
  public function  getTableDescr($tableName){	
  	$res = $this->pdo->prepare("DESCRIBE " .  $tableName) ;
  	$res->execute();	
		return $res->fetchAll(PDO::FETCH_CLASS); 
  }
}	